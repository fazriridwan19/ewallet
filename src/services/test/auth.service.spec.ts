import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { CreateProfileDto } from '../../entities/dto/create-profile.dto';
import { AuthService } from '../auth.service';
import { ProfileService } from '../profile.service';
import { JwtService } from '@nestjs/jwt';

describe('Auth Service', () => {
  let service: AuthService;
  let moduleRef: TestingModule;

  const mockSaveResult: any = {
    id: 3,
    firstName: 'Ursklap 2',
    lastName: 'San 2',
    birth: null,
    email: 'ursklap2@gmail.com',
    phone: '085352307022',
  };

  const mockCreateProfileDto: CreateProfileDto = {
    firstName: 'Ursklap 2',
    lastName: 'San 2',
    email: 'ursklap2@gmail.com',
    phone: '085352307022',
    birth: '19-09-2000',
    username: 'ursklap2',
    password: 'ursklap2',
  };

  const profileService = {
    create: jest.fn(() => {}),
  };

  const configService = {
    get: jest.fn(() => 'Test'),
  };

  const jwtService = {
    sign: jest.fn(() => 'Test'),
  };

  beforeEach(async () => {
    moduleRef = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: ProfileService, useValue: profileService },
        { provide: ConfigService, useValue: configService },
        { provide: JwtService, useValue: jwtService },
      ],
    }).compile();
    moduleRef.useLogger(false);
    service = moduleRef.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('registration', () => {
    it('should create new user profile', async () => {
      jest.spyOn(profileService, 'create').mockImplementation(() => {
        return mockSaveResult;
      });

      const serviceSpy = jest
        .spyOn(service, 'registration')
        .mockReturnValue(mockSaveResult);

      const result = await service.registration(mockCreateProfileDto);

      expect(serviceSpy).toHaveBeenCalledWith(mockCreateProfileDto);
      expect(result).toEqual(mockSaveResult);
    });
  });
});
