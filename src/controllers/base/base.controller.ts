import { BaseService } from 'src/services/base/base.service';

export class BaseController<E> {
  constructor(private readonly service: BaseService<E>) {}
}
